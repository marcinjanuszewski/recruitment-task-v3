import { expect } from "chai";
import "mocha";
import * as request from "supertest";

describe("/api/movie integration", () => {
  it("find movies without params, should return one random movie", async () => {
    const res = await request(global.container.resolve("app"))
      .get("/api/movie/find-movies")
      .expect(200)
      .expect("Content-Type", /json/);

    expect(res.body).instanceOf(Array);
    expect(res.body).length(1);
  });

  it("find movies with negative duration, should return validation error", async () => {
    const res = await request(global.container.resolve("app"))
      .get("/api/movie/find-movies")
      .query({ duration: -1 })
      .expect(400)
      .expect("Content-Type", /json/);

    expect(res.body).to.have.property("error");
    expect(res.body.error.duration.id.toString()).equals("validation.number.min");
  });

  it("find movies with empty genres param, should return validation error", async () => {
    const res = await request(global.container.resolve("app"))
      .get("/api/movie/find-movies")
      .query({ genres: "" })
      .expect(400)
      .expect("Content-Type", /json/);

    expect(res.body).to.have.property("error");
    expect(res.body.error.genres.id.toString()).equals("validation.string.empty");
  });

  it("add movie without required fields, should return required fields validation error", async () => {
    const res = await request(global.container.resolve("app"))
      .post("/api/movie/add-movie")
      .send({
        genres: ["Comedy"],
        title: "The lovely doggy doggo",
      })
      .expect(400)
      .expect("Content-Type", /json/);

    expect(res.body).to.have.property("error");
    expect(res.body.error.year.id.toString()).equals("validation.any.required");
    expect(res.body.error.runtime.id.toString()).equals("validation.any.required");
    expect(res.body.error.director.id.toString()).equals("validation.any.required");
  });

  it("add movie with year and runtime as not numeric, should return required fields validation error", async () => {
    const res = await request(global.container.resolve("app"))
      .post("/api/movie/add-movie")
      .send({
        genres: ["Comedy"],
        title: "The lovely doggy doggo",
        director: "Janusz Tracz",
        year: "thisIsYear",
        runtime: "andThisIsDuration",
      })
      .expect(400)
      .expect("Content-Type", /json/);

    expect(res.body).to.have.property("error");
    expect(res.body.error.year.id.toString()).equals("validation.number.base");
    expect(res.body.error.runtime.id.toString()).equals("validation.number.base");
  });

  it("add movie with too long title, should return title length validation error", async () => {
    const res = await request(global.container.resolve("app"))
      .post("/api/movie/add-movie")
      .send({
        genres: ["Comedy"],
        title:
          "Lqq6ZNIA1Glw1scYYGN9zMMXmJPuZzeSiwjsAKwOs1llH3KK58NgebM9jvIDfqexc7tgegrY82oi81IWOtJdVHFHhvA2NlT8UNhDg1raYUiJgSGfPf1h29HvFjb9DO5l535CfPCgdCvubYWm8d7rrPfehbHyQsAuZnwVC1B4eJEvLaYkPNQUfkZFB26rbVMW31Ks0XCeStgCAE3lYhz8FYJcONSUS0kJy5Z0hGFc4m6bcjVY2GSpUgCG53HEhrBl",
        year: "2020",
        runtime: "175",
        director: "Janusz Tracz",
      })
      .expect(400)
      .expect("Content-Type", /json/);

    expect(res.body).to.have.property("error");
    expect(res.body.error.title.id.toString()).equals("validation.string.max");
  });

  it("add movie with undefined genre, should return unexpectedValues error", async () => {
    const res = await request(global.container.resolve("app"))
      .post("/api/movie/add-movie")
      .send({
        genres: ["Comedy", "TheGenreThatDoesntExist"],
        title: "The lovely doggy doggo",
        year: 2020,
        runtime: 175,
        director: "Janusz Tracz",
      })
      .expect(400)
      .expect("Content-Type", /json/);

    expect(res.body).to.have.property("error");
    expect(res.body.error.id.toString()).equals("error.validation.array.unexpectedValues");
  });

  it("add movie with empty genres array, should return genres validation error", async () => {
    const res = await request(global.container.resolve("app"))
      .post("/api/movie/add-movie")
      .send({
        genres: [],
        title:
          "Lqq6ZNIA1Glw1scYYGN9zMMXmJPuZzeSiwjsAKwOs1llH3KK58NgebM9jvIDfqexc7tgegrY82oi81IWOtJdVHFHhvA2NlT8UNhDg1raYUiJgSGfPf1h29HvFjb9DO5l535CfPCgdCvubYWm8d7rrPfehbHyQsAuZnwVC1B4eJEvLaYkPNQUfkZFB26rbVMW31Ks0XCeStgCAE3lYhz8FYJcONSUS0kJy5Z0hGFc4m6bcjVY2GSpUgCG53HEhrBl",
        year: "2020",
        runtime: "175",
        director: "Janusz Tracz",
      })
      .expect(400)
      .expect("Content-Type", /json/);

    expect(res.body).to.have.property("error");
    expect(res.body.error.genres.id.toString()).equals("validation.array.min");
  });
});
