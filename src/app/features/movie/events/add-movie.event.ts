import { Command } from "../../../../shared/command-bus";
import { Event } from "../../../../shared/event-dispatcher/index";
import { AddMovieCommandPayload } from "../commands/add-movie.command";

export default class AddMovieEvent implements Event {
  static eventName: string = "AddMovie";

  public payload: Command<AddMovieCommandPayload>;

  get name() {
    return AddMovieEvent.eventName;
  }

  public constructor(command: Command<AddMovieCommandPayload>) {
    this.payload = command;
  }
}
