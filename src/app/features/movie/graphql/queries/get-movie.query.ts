import { QueryContext } from "../../../../../graphql/resolvers";
import { GetMovieQuery } from "../../queries/get-movie";

export const getMovieQuery = async (parent: any, args: any, context: QueryContext) => {
  const { result } = await context.queryBus.execute(new GetMovieQuery(args));
  return result;
};
