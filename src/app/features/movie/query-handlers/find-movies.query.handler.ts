import { QueryHandler } from "../../../../shared/query-bus";
import { MovieRepository } from "../repositories/movie.repository";
import { FIND_MOVIES_QUERY_TYPE, FindMoviesQuery, FindMoviesQueryResult } from "../queries/find-movies";

interface FindMoviesDependencies {
  movieRepository: MovieRepository;
}

export default class FindMoviesQueryHandler implements QueryHandler<FindMoviesQuery, FindMoviesQueryResult> {
  public queryType: string = FIND_MOVIES_QUERY_TYPE;

  constructor(public dependencies: FindMoviesDependencies) {}

  async execute(query: FindMoviesQuery): Promise<FindMoviesQueryResult> {
    const { movieRepository } = this.dependencies;

    const result = await movieRepository.findMovies(query.payload);

    return new FindMoviesQueryResult(result);
  }
}
