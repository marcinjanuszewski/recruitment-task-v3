import { classToPlain } from "class-transformer";
import { QueryHandler } from "../../../../shared/query-bus";
import { GET_MOVIE_QUERY_TYPE, GetMovieQuery, GetMovieQueryResult } from "../queries/get-movie";
import { MovieRepository } from "../repositories/movie.repository";

interface GetMovieDependencies {
  movieRepository: MovieRepository;
}

export default class GetMovieQueryHandler implements QueryHandler<GetMovieQuery, GetMovieQueryResult> {
  public queryType: string = GET_MOVIE_QUERY_TYPE;

  constructor(public dependencies: GetMovieDependencies) {}

  async execute(query: GetMovieQuery): Promise<GetMovieQueryResult> {
    const { movieRepository } = this.dependencies;
    const result = await movieRepository.getMovie(query.payload.id);

    return new GetMovieQueryResult(classToPlain(result));
  }
}
