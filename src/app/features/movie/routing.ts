import * as express from "express";
import { Action } from "../../../shared/http/types";

import { findMoviesActionValidation } from "./actions/find-movies.action";
import { addMovieActionValidation } from "./actions/add-movie.action";
// VALIDATION_IMPORTS

export interface MovieRoutingDependencies {
  findMoviesAction: Action;
  addMovieAction: Action;
  // ACTIONS_IMPORTS
}

export const movieRouting = (actions: MovieRoutingDependencies) => {
  const router = express.Router();

  router.get(
    "/find-movies",
    [findMoviesActionValidation],
    actions.findMoviesAction.invoke.bind(actions.findMoviesAction),
  );
  router.post("/add-movie", [addMovieActionValidation], actions.addMovieAction.invoke.bind(actions.addMovieAction));
  // ACTIONS_SETUP

  return router;
};
