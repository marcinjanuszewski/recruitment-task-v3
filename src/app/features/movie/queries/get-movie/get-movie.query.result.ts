import { QueryResult } from "../../../../../shared/query-bus";

export class GetMovieQueryResult implements QueryResult<any> {
  constructor(public result: any) {}
}
