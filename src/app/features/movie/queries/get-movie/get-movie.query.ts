import { Query } from "../../../../../shared/query-bus";

export const GET_MOVIE_QUERY_TYPE = "movie/GET_MOVIE";

export interface GetMovieQueryPayload {
  id: number;
}

export class GetMovieQuery implements Query<GetMovieQueryPayload> {
  public type: string = GET_MOVIE_QUERY_TYPE;

  constructor(public payload: GetMovieQueryPayload) {}
}
