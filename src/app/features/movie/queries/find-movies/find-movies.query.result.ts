import { QueryResult } from "../../../../../shared/query-bus";

export class FindMoviesQueryResult implements QueryResult<any> {
  constructor(public result: any) {}
}
