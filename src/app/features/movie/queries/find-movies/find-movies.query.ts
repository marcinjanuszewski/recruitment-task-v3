import { Query } from "../../../../../shared/query-bus";

export const FIND_MOVIES_QUERY_TYPE = "movie/FIND_MOVIES";

export interface FindMoviesQueryPayload {
  duration: number;
  genres: string[];
}

export class FindMoviesQuery implements Query<FindMoviesQueryPayload> {
  public type: string = FIND_MOVIES_QUERY_TYPE;

  constructor(public payload: FindMoviesQueryPayload) {}
}
