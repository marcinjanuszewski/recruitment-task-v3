import { plainToClass } from "class-transformer";
import * as fs from "fs";
import { HttpError } from "../../../../errors/http.error";
import { MovieSortHelper } from "../helpers/movie-sort.helper";
import { AddMovieDtoProps } from "../models/add-movie-dto.model";
import { DbFile } from "../models/db-file.model";
import { FindMovieDto } from "../models/find-movie.dto";
import { Movie } from "../models/movie.model";

export interface MovieRepository {
  addMovie(movie: AddMovieDtoProps): Promise<Movie>;
  getMovie(id: number): Promise<Movie>;
  findMovies(query: FindMovieDto): Promise<Movie[]>;
}

export class MovieFileRepository implements MovieRepository {
  public async addMovie(movie: AddMovieDtoProps): Promise<Movie> {
    const file = await this.loadMoviesFromFile();
    if (
      !movie.genres.every((item: string) =>
        file.genres.map((x: string) => x.toLowerCase()).includes(item.toLowerCase()),
      )
    ) {
      throw new HttpError("error.validation.array.unexpectedValues", 400);
    }

    const lastId = Math.max(...file.movies.map((m: Movie) => m.id), 0);

    const movieModel = Movie.create({ id: lastId + 1, ...movie });

    file.movies.push(movieModel);
    fs.writeFileSync("/data/db.json", JSON.stringify(file, undefined, 2));

    return movieModel;
  }

  public async getMovie(id: number): Promise<Movie> {
    const content = await this.loadMoviesFromFile();
    const { movies } = content;

    const filteredMovies = movies.filter((x: Movie) => x.id === id);
    if (filteredMovies.length === 0) {
      throw new HttpError(`Movie (id: ${id}) not found`, 404);
    }

    if (filteredMovies.length > 1) {
      throw new HttpError("Primary key violated", 500);
    }

    return filteredMovies[0];
  }

  public async findMovies(query: FindMovieDto): Promise<Movie[]> {
    const content = await this.loadMoviesFromFile();
    let { movies } = content;

    if (!Number.isNaN(query.duration) && query.duration > 0) {
      movies = movies.filter(
        (x: Movie) => Number(x.runtime) >= query.duration - 10 && Number(x.runtime) <= query.duration + 10,
      );
    }

    if (movies.length === 0) {
      return [];
    }

    if (query.genres === undefined || query.genres.length === 0) {
      const id = Math.floor(Math.random() * movies.length);
      return [movies[id]];
    }

    return MovieSortHelper.sort(movies, query.genres);
  }

  private async loadMoviesFromFile(): Promise<DbFile> {
    const content = fs.readFileSync("/data/db.json");

    const json = JSON.parse(content.toString());
    return plainToClass(DbFile, json);
  }
}
