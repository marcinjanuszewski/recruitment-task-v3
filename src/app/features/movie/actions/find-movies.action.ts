import { Request, Response } from "express";
import { celebrate, Joi } from "celebrate";
import { ApiOperationGet, ApiPath, SwaggerDefinitionConstant } from "swagger-express-ts";
import { QueryBus } from "../../../../shared/query-bus";
import { FindMoviesQuery } from "../queries/find-movies";
import { Action } from "../../../../shared/http/types";

export interface FindMoviesActionDependencies {
  queryBus: QueryBus;
}

export const findMoviesActionValidation = celebrate(
  {
    headers: Joi.object(),
    query: {
      duration: Joi.number().integer().min(1),
      genres: Joi.string().trim().min(1).allow(null),
    },
  },
  { abortEarly: false },
);

@ApiPath({
  path: "/api",
  name: "movie",
})
class FindMoviesAction implements Action {
  constructor(private dependencies: FindMoviesActionDependencies) {}

  @ApiOperationGet({
    path: "/movie/find-movies",
    description: "Find movies query",
    parameters: {
      query: {
        duration: {
          type: "number",
          minimum: 1,
        },
        // this actually should be ARRAY with string items but swagger-express-ts doesnt support it
        genres: {
          type: "string",
          description: "Comma separated genres like: comedy,fantasy",
        },
      },
    },
    responses: {
      200: {
        description: "Success",
        type: SwaggerDefinitionConstant.Response.Type.ARRAY,
        model: "Movie",
      },
      400: {
        description: "Validation error",
      },
      500: {
        description: "Internal Server Error",
      },
    },
  })
  async invoke(req: Request, res: Response) {
    const queryResult = await this.dependencies.queryBus.execute(
      new FindMoviesQuery({
        duration: Number(req.query.duration),
        genres: req.query.genres === undefined ? [] : req.query.genres.toString().split(","),
      }),
    );

    res.json(queryResult.result);
  }
}
export default FindMoviesAction;
