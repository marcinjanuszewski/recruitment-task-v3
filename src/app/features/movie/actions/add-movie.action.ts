import { Request, Response } from "express";
import { celebrate, Joi } from "celebrate";
import { ApiOperationPost, ApiPath } from "swagger-express-ts";
import { CommandBus } from "../../../../shared/command-bus";
import { AddMovieCommand } from "../commands/add-movie.command";
import { Action } from "../../../../shared/http/types";

export interface AddMovieActionDependencies {
  commandBus: CommandBus;
}

export const addMovieActionValidation = celebrate(
  {
    body: {
      genres: Joi.array().required().min(1),
      title: Joi.string().required().max(255),
      year: Joi.number().integer().positive().required().raw(),
      runtime: Joi.number().integer().positive().required().raw(),
      director: Joi.string().required().max(255),
      actors: Joi.string(),
      plot: Joi.string(),
      posterUrl: Joi.string(),
    },
    headers: Joi.object(),
  },
  { abortEarly: false },
);

@ApiPath({
  path: "/api",
  name: "movie",
})
class AddMovieAction implements Action {
  constructor(private dependencies: AddMovieActionDependencies) {}

  @ApiOperationPost({
    path: "/movie/add-movie",
    description: "Description",
    parameters: {
      body: {
        model: "AddMovieDto",
        required: true,
      },
    },
    responses: {
      201: {
        description: "Movie successfully added",
        model: "Movie",
      },
      400: {
        description: "Validation error",
      },
      500: {
        description: "Internal Server Error",
      },
    },
  })
  async invoke({ body }: Request, res: Response) {
    const commandResult = await this.dependencies.commandBus.execute(
      new AddMovieCommand({
        movie: body,
      }),
    );

    res.status(201).json(commandResult.result);
  }
}
export default AddMovieAction;
