import { CommandHandler } from "../../../../shared/command-bus";
import { ADD_MOVIE_COMMAND_TYPE, AddMovieCommand } from "../commands/add-movie.command";
import { EventDispatcher } from "../../../../shared/event-dispatcher";
import AddMovieEvent from "../events/add-movie.event";
import { MovieRepository } from "../repositories/movie.repository";

export interface AddMovieHandlerDependencies {
  eventDispatcher: EventDispatcher;
  movieRepository: MovieRepository;
}

export default class AddMovieHandler implements CommandHandler<AddMovieCommand> {
  public commandType: string = ADD_MOVIE_COMMAND_TYPE;

  constructor(private dependencies: AddMovieHandlerDependencies) {}

  async execute(command: AddMovieCommand) {
    const { movieRepository } = this.dependencies;
    const result = await movieRepository.addMovie(command.payload.movie);

    await this.dependencies.eventDispatcher.dispatch(new AddMovieEvent(command));

    return {
      result,
    };
  }
}
