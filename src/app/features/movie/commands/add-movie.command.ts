import { Command } from "../../../../shared/command-bus";
import { AddMovieDtoProps } from "../models/add-movie-dto.model";

export const ADD_MOVIE_COMMAND_TYPE = "movie/ADD_MOVIE";

export interface AddMovieCommandPayload {
  movie: AddMovieDtoProps;
}

export class AddMovieCommand implements Command<AddMovieCommandPayload> {
  public type: string = ADD_MOVIE_COMMAND_TYPE;

  constructor(public payload: AddMovieCommandPayload) {}
}
