import { expect } from "chai";
import "mocha";
import { MovieProps } from "../models/movie.model";
import { MovieSortHelper } from "./movie-sort.helper";

describe("movie sort unit tests", () => {
  [
    {
      genres: ["Romance", "COMEDY", "drama"],
      expectedOrder: [50, 38, 53, 109, 45, 1, 4, 14, 42, 60, 62, 63, 85, 86, 117, 119, 122, 125, 127, 131, 36, 114],
    },

    {
      genres: ["comedy", "fantasy"],
      expectedOrder: [1, 38, 4, 14, 42, 45, 50, 60, 62, 63, 85, 86, 117, 119, 122, 125, 127, 131],
    },
    {
      genres: ["romance", "crime"],
      expectedOrder: [38, 50, 53, 109, 36, 86, 114, 117],
    },
  ].forEach((test) => {
    it("order movies by genres", async () => {
      // eslint-disable-next-line
      const movies = Object.assign(new Array<MovieProps>(), moviesToTest);

      const orderedMovies = MovieSortHelper.sort(movies, test.genres);

      expect(orderedMovies.map((x) => x.id)).deep.equal(test.expectedOrder);
    });
  });
});

// this could/should be exported to external .json file to load
/* eslint-disable @typescript-eslint/quotes */
const moviesToTest = [
  {
    id: 1,
    title: "Beetlejuice",
    year: "1988",
    runtime: "92",
    genres: ["Comedy", "Fantasy"],
    director: "Tim Burton",
    actors: "Alec Baldwin, Geena Davis, Annie McEnroe, Maurice Page",
    plot:
      'A couple of recently deceased ghosts contract the services of a ""bio-exorcist"" in order to remove the obnoxious new owners of their house.',
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTUwODE3MDE0MV5BMl5BanBnXkFtZTgwNTk1MjI4MzE@._V1_SX300.jpg",
  },
  {
    id: 4,
    title: "Crocodile Dundee",
    year: "1986",
    runtime: "97",
    genres: ["Adventure", "Comedy"],
    director: "Peter Faiman",
    actors: "Paul Hogan, Linda Kozlowski, John Meillon, David Gulpilil",
    plot:
      "An American reporter goes to the Australian outback to meet an eccentric crocodile poacher and invites him to New York City.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTg0MTU1MTg4NF5BMl5BanBnXkFtZTgwMDgzNzYxMTE@._V1_SX300.jpg",
  },
  {
    id: 14,
    title: "Planet 51",
    year: "2009",
    runtime: "91",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Jorge Blanco, Javier Abad, Marcos Martínez",
    actors: "Jessica Biel, John Cleese, Gary Oldman, Dwayne Johnson",
    plot:
      "An alien civilization is invaded by Astronaut Chuck Baker, who believes that the planet was uninhabited. Wanted by the military, Baker must get back to his ship before it goes into orbit without him.",
    posterUrl: "http://ia.media-imdb.com/images/M/MV5BMTUyOTAyNTA5Ml5BMl5BanBnXkFtZTcwODU2OTM0Mg@@._V1_SX300.jpg",
  },
  {
    id: 36,
    title: "Reservoir Dogs",
    year: "1992",
    runtime: "99",
    genres: ["Crime", "Drama", "Thriller"],
    director: "Quentin Tarantino",
    actors: "Harvey Keitel, Tim Roth, Michael Madsen, Chris Penn",
    plot:
      "After a simple jewelry heist goes terribly wrong, the surviving criminals begin to suspect that one of them is a police informant.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BNjE5ZDJiZTQtOGE2YS00ZTc5LTk0OGUtOTg2NjdjZmVlYzE2XkEyXkFqcGdeQXVyMzM4MjM0Nzg@._V1_SX300.jpg",
  },
  {
    id: 38,
    title: "Midnight in Paris",
    year: "2011",
    runtime: "94",
    genres: ["Comedy", "Fantasy", "Romance"],
    director: "Woody Allen",
    actors: "Owen Wilson, Rachel McAdams, Kurt Fuller, Mimi Kennedy",
    plot:
      "While on a trip to Paris with his fiancée's family, a nostalgic screenwriter finds himself mysteriously going back to the 1920s everyday at midnight.",
    posterUrl: "http://ia.media-imdb.com/images/M/MV5BMTM4NjY1MDQwMl5BMl5BanBnXkFtZTcwNTI3Njg3NA@@._V1_SX300.jpg",
  },
  {
    id: 42,
    title: "The Hangover",
    year: "2009",
    runtime: "100",
    genres: ["Comedy"],
    director: "Todd Phillips",
    actors: "Bradley Cooper, Ed Helms, Zach Galifianakis, Justin Bartha",
    plot:
      "Three buddies wake up from a bachelor party in Las Vegas, with no memory of the previous night and the bachelor missing. They make their way around the city in order to find their friend before his wedding.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTU1MDA1MTYwMF5BMl5BanBnXkFtZTcwMDcxMzA1Mg@@._V1_SX300.jpg",
  },
  {
    id: 45,
    title: "Mary and Max",
    year: "2009",
    runtime: "92",
    genres: ["Animation", "Comedy", "Drama"],
    director: "Adam Elliot",
    actors: "Toni Collette, Philip Seymour Hoffman, Barry Humphries, Eric Bana",
    plot:
      "A tale of friendship between two unlikely pen pals: Mary, a lonely, eight-year-old girl living in the suburbs of Melbourne, and Max, a forty-four-year old, severely obese man living in New York.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ1NDIyNTA1Nl5BMl5BanBnXkFtZTcwMjc2Njk3OA@@._V1_SX300.jpg",
  },
  {
    id: 50,
    title: "The Artist",
    year: "2011",
    runtime: "100",
    genres: ["Comedy", "Drama", "Romance"],
    director: "Michel Hazanavicius",
    actors: "Jean Dujardin, Bérénice Bejo, John Goodman, James Cromwell",
    plot:
      "A silent movie star meets a young dancer, but the arrival of talking pictures sends their careers in opposite directions.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMzk0NzQxMTM0OV5BMl5BanBnXkFtZTcwMzU4MDYyNQ@@._V1_SX300.jpg",
  },
  {
    id: 53,
    title: "Vicky Cristina Barcelona",
    year: "2008",
    runtime: "96",
    genres: ["Drama", "Romance"],
    director: "Woody Allen",
    actors: "Rebecca Hall, Scarlett Johansson, Christopher Evan Welch, Chris Messina",
    plot:
      "Two girlfriends on a summer holiday in Spain become enamored with the same painter, unaware that his ex-wife, with whom he has a tempestuous relationship, is about to re-enter the picture.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTU2NDQ4MTg2MV5BMl5BanBnXkFtZTcwNDUzNjU3MQ@@._V1_SX300.jpg",
  },
  {
    id: 60,
    title: "Despicable Me 2",
    year: "2013",
    runtime: "98",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Pierre Coffin, Chris Renaud",
    actors: "Steve Carell, Kristen Wiig, Benjamin Bratt, Miranda Cosgrove",
    plot:
      "When Gru, the world's most super-bad turned super-dad has been recruited by a team of officials to stop lethal muscle and a host of Gru's own, He has to fight back with new gadgetry, cars, and more minion madness.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjExNjAyNTcyMF5BMl5BanBnXkFtZTgwODQzMjQ3MDE@._V1_SX300.jpg",
  },
  {
    id: 62,
    title: "Madagascar",
    year: "2005",
    runtime: "86",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Eric Darnell, Tom McGrath",
    actors: "Ben Stiller, Chris Rock, David Schwimmer, Jada Pinkett Smith",
    plot:
      "Spoiled by their upbringing with no idea what wild life is really like, four animals from New York Central Zoo escape, unwittingly assisted by four absconding penguins, and find themselves in Madagascar, among a bunch of merry lemurs",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY4NDUwMzQxMF5BMl5BanBnXkFtZTcwMDgwNjgyMQ@@._V1_SX300.jpg",
  },
  {
    id: 63,
    title: "Madagascar 3: Europe's Most Wanted",
    year: "2012",
    runtime: "93",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Eric Darnell, Tom McGrath, Conrad Vernon",
    actors: "Ben Stiller, Chris Rock, David Schwimmer, Jada Pinkett Smith",
    plot:
      "Alex, Marty, Gloria and Melman are still fighting to get home to their beloved Big Apple. Their journey takes them through Europe where they find the perfect cover: a traveling circus, which they reinvent - Madagascar style.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTM2MTIzNzk2MF5BMl5BanBnXkFtZTcwMDcwMzQxNw@@._V1_SX300.jpg",
  },
  {
    id: 85,
    title: "I-See-You.Com",
    year: "2006",
    runtime: "92",
    genres: ["Comedy"],
    director: "Eric Steven Stahl",
    actors: "Beau Bridges, Rosanna Arquette, Mathew Botuchis, Shiri Appleby",
    plot:
      "A 17-year-old boy buys mini-cameras and displays the footage online at I-see-you.com. The cash rolls in as the site becomes a major hit. Everyone seems to have fun until it all comes crashing down....",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTYwMDUzNzA5Nl5BMl5BanBnXkFtZTcwMjQ2Njk3MQ@@._V1_SX300.jpg",
  },
  {
    id: 86,
    title: "The Grand Budapest Hotel",
    year: "2014",
    runtime: "99",
    genres: ["Adventure", "Comedy", "Crime"],
    director: "Wes Anderson",
    actors: "Ralph Fiennes, F. Murray Abraham, Mathieu Amalric, Adrien Brody",
    plot:
      "The adventures of Gustave H, a legendary concierge at a famous hotel from the fictional Republic of Zubrowka between the first and second World Wars, and Zero Moustafa, the lobby boy who becomes his most trusted friend.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMzM5NjUxOTEyMl5BMl5BanBnXkFtZTgwNjEyMDM0MDE@._V1_SX300.jpg",
  },
  {
    id: 109,
    title: "Before Sunset",
    year: "2004",
    runtime: "80",
    genres: ["Drama", "Romance"],
    director: "Richard Linklater",
    actors: "Ethan Hawke, Julie Delpy, Vernon Dobtcheff, Louise Lemoine Torrès",
    plot:
      "Nine years after Jesse and Celine first met, they encounter each other again on the French leg of Jesse's book tour.",
    posterUrl: "http://ia.media-imdb.com/images/M/MV5BMTQ1MjAwNTM5Ml5BMl5BanBnXkFtZTYwNDM0MTc3._V1_SX300.jpg",
  },
  {
    id: 114,
    title: "12 Angry Men",
    year: "1957",
    runtime: "96",
    genres: ["Crime", "Drama"],
    director: "Sidney Lumet",
    actors: "Martin Balsam, John Fiedler, Lee J. Cobb, E.G. Marshall",
    plot:
      "A jury holdout attempts to prevent a miscarriage of justice by forcing his colleagues to reconsider the evidence.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BODQwOTc5MDM2N15BMl5BanBnXkFtZTcwODQxNTEzNA@@._V1_SX300.jpg",
  },
  {
    id: 117,
    title: "Big Nothing",
    year: "2006",
    runtime: "86",
    genres: ["Comedy", "Crime", "Thriller"],
    director: "Jean-Baptiste Andrea",
    actors: "David Schwimmer, Simon Pegg, Alice Eve, Natascha McElhone",
    plot: "A frustrated, unemployed teacher joining forces with a scammer and his girlfriend in a blackmailing scheme.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY5NTc2NjYwOV5BMl5BanBnXkFtZTcwMzk5OTY0MQ@@._V1_SX300.jpg",
  },
  {
    id: 119,
    title: "Shrek 2",
    year: "2004",
    runtime: "93",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Andrew Adamson, Kelly Asbury, Conrad Vernon",
    actors: "Mike Myers, Eddie Murphy, Cameron Diaz, Julie Andrews",
    plot:
      "Princess Fiona's parents invite her and Shrek to dinner to celebrate her marriage. If only they knew the newlyweds were both ogres.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTk4MTMwNjI4M15BMl5BanBnXkFtZTcwMjExMzUyMQ@@._V1_SX300.jpg",
  },
  {
    id: 122,
    title: "Shrek",
    year: "2001",
    runtime: "90",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Andrew Adamson, Vicky Jenson",
    actors: "Mike Myers, Eddie Murphy, Cameron Diaz, John Lithgow",
    plot:
      "After his swamp is filled with magical creatures, an ogre agrees to rescue a princess for a villainous lord in order to get his land back.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTk2NTE1NTE0M15BMl5BanBnXkFtZTgwNjY4NTYxMTE@._V1_SX300.jpg",
  },
  {
    id: 125,
    title: "Shrek Forever After",
    year: "2010",
    runtime: "93",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Mike Mitchell",
    actors: "Mike Myers, Eddie Murphy, Cameron Diaz, Antonio Banderas",
    plot:
      "Rumpelstiltskin tricks a mid-life crisis burdened Shrek into allowing himself to be erased from existence and cast in a dark alternate timeline where Rumpel rules supreme.",
    posterUrl: "http://ia.media-imdb.com/images/M/MV5BMTY0OTU1NzkxMl5BMl5BanBnXkFtZTcwMzI2NDUzMw@@._V1_SX300.jpg",
  },
  {
    id: 127,
    title: "Despicable Me",
    year: "2010",
    runtime: "95",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Pierre Coffin, Chris Renaud",
    actors: "Steve Carell, Jason Segel, Russell Brand, Julie Andrews",
    plot:
      "When a criminal mastermind uses a trio of orphan girls as pawns for a grand scheme, he finds their love is profoundly changing him for the better.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY3NjY0MTQ0Nl5BMl5BanBnXkFtZTcwMzQ2MTc0Mw@@._V1_SX300.jpg",
  },
  {
    id: 131,
    title: "Ice Age",
    year: "2002",
    runtime: "81",
    genres: ["Animation", "Adventure", "Comedy"],
    director: "Chris Wedge, Carlos Saldanha",
    actors: "Ray Romano, John Leguizamo, Denis Leary, Goran Visnjic",
    plot:
      "Set during the Ice Age, a sabertooth tiger, a sloth, and a wooly mammoth find a lost human infant, and they try to return him to his tribe.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjEyNzI1ODA0MF5BMl5BanBnXkFtZTYwODIxODY3._V1_SX300.jpg",
  },
];
/* eslint-enable @typescript-eslint/quotes */
