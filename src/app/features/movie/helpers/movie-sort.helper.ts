import { MovieProps } from "../models/movie.model";

export class MovieSortHelper {
  public static sort(movies: MovieProps[], genres: string[]) {
    const caseInvariantGenres = genres.map((x: string) => x.toLowerCase());

    const movieWithGenreMatchIds = movies.map((m: MovieProps) => {
      return {
        movie: m,
        genreIndexes: m.genres
          .map((x: string) => caseInvariantGenres.indexOf(x.toLowerCase()))
          .filter((x: number) => x >= 0),
      };
    });

    return movieWithGenreMatchIds
      .filter((x: any) => x.genreIndexes.length > 0)
      .sort((x: any, y: any) => this.compareTwoMoviesByGenres(caseInvariantGenres, x, y))
      .map((x: any) => x.movie);
  }

  private static compareTwoMoviesByGenres(genres: string[], x: any, y: any) {
    // firstly compare two movies by genres matches - if one movie has more matches that the other, then return result
    const comparer = y.genreIndexes.length - x.genreIndexes.length;
    if (comparer !== 0) {
      return comparer;
    }

    // if movies have the same matches count, then sort them by 'genre priority'
    const genrePriority =
      this.calculateGenrePriority(genres, y.genreIndexes) - this.calculateGenrePriority(genres, x.genreIndexes);
    if (genrePriority !== 0) {
      return genrePriority;
    }

    // if movies have the same genre priority, then order this by id
    return x.id - y.id;
  }

  // calculated genre Priority by order of genres in 'genres: string[]' using binary calculated priority. the first genre has highest priority
  private static calculateGenrePriority(genres: string[], genreIndexes: number[]): number {
    return genreIndexes.reduce((sum: number, current: number) => sum + 2 ** (genres.length - current), 0);
  }
}
