export interface FindMovieDto {
  duration: number;
  genres: string[];
}
