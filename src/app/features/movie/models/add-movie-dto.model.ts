// this file should be named as add-movie.dto.ts actually, but swagger ignores .dto.(ts/js) files

import { ApiModel, ApiModelProperty, SwaggerDefinitionConstant } from "swagger-express-ts";

export interface AddMovieDtoProps {
  genres: string[];
  title: string;
  year: string;
  runtime: string;
  director: string;
  actors: string;
  plot: string;
  posterUrl: string;
}

@ApiModel({
  description: "Add movie model",
  name: "AddMovieDto",
})
export class AddMovieDto implements AddMovieDtoProps {
  public static create(data: Partial<AddMovieDtoProps>): AddMovieDto {
    const object = new AddMovieDto();
    Object.assign(object, data);
    return object;
  }

  @ApiModelProperty({ itemType: "string", type: SwaggerDefinitionConstant.Response.Type.ARRAY, required: true })
  genres: string[];

  @ApiModelProperty({ required: true })
  title: string;

  @ApiModelProperty({ required: true })
  year: string;

  @ApiModelProperty({ required: true })
  runtime: string;

  @ApiModelProperty({ required: true })
  director: string;

  @ApiModelProperty()
  actors: string;

  @ApiModelProperty()
  plot: string;

  @ApiModelProperty()
  posterUrl: string;
}
