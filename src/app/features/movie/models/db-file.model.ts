import { Movie } from "./movie.model";

export class DbFile {
  genres: string[];

  movies: Movie[];
}
