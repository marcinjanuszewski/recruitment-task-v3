import { ApiModel, ApiModelProperty, SwaggerDefinitionConstant } from "swagger-express-ts";
import { AddMovieDtoProps } from "./add-movie-dto.model";

export interface MovieProps extends AddMovieDtoProps {
  id: number;
}

@ApiModel({
  description: "Movie model",
  name: "Movie",
})
export class Movie implements MovieProps {
  public static create(data: Partial<MovieProps>): Movie {
    const movie = new Movie();
    Object.assign(movie, data);
    return movie;
  }

  @ApiModelProperty()
  id: number;

  @ApiModelProperty({ itemType: "string", type: SwaggerDefinitionConstant.Response.Type.ARRAY })
  genres: string[];

  @ApiModelProperty()
  title: string;

  @ApiModelProperty()
  year: string;

  @ApiModelProperty()
  runtime: string;

  @ApiModelProperty()
  director: string;

  @ApiModelProperty()
  actors: string;

  @ApiModelProperty()
  plot: string;

  @ApiModelProperty()
  posterUrl: string;
}
