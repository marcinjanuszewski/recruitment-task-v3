import * as express from "express";

export interface RoutingDependencies {  
  movieRouting: express.Router;
  // ROUTES_INTERFACE
}

export const createRouter = ({
  movieRouting,
  // ROUTES_DEPENDENCIES
}: RoutingDependencies) => {
  const router = express.Router();

  router.use("/movie", movieRouting);
  // ROUTES_CONFIG
  return router;
};
