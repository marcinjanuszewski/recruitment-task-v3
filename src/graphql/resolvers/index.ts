import { Resolvers } from "../types";
import { CommandBus, QueryBus } from "../../shared";
import { getMovieQuery } from "../../app/features/movie/graphql/queries/get-movie.query";
// QUERY_IMPORTS
// MUTATION_IMPORTS

export type MutationContext = {
  commandBus: CommandBus;
};

export type QueryContext = {
  queryBus: QueryBus;
};

interface ResolversDependencies {}

export const createResolvers = (_dependencies: ResolversDependencies): Resolvers => {
  // Provide resolver functions for your schema fields
  const resolvers = {
    Query: {
      getMovie: getMovieQuery,
      // GRAPHQL_QUERIES
    },
  };

  return resolvers;
};
