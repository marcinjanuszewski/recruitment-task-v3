import { AwilixContainer } from "awilix";
import * as awilix from "awilix";
import { MovieFileRepository } from "../app/features/movie/repositories/movie.repository";

export async function registerRepositories(container: AwilixContainer) {
  container.register({
    movieRepository: awilix.asValue(new MovieFileRepository()),
  });

  return container;
}
