import { AwilixContainer } from "awilix";
import * as awilix from "awilix";
import { asArray } from "../shared/awilix-resolvers";

import FindMoviesQueryHandler from "../app/features/movie/query-handlers/find-movies.query.handler";
import GetMovieQueryHandler from "../app/features/movie/query-handlers/get-movie.query.handler";
// HANDLERS_IMPORTS

export async function registerQueryHandlers(container: AwilixContainer) {
  container.register({
    queryHandlers: asArray<any>([
      awilix.asClass(FindMoviesQueryHandler),
      awilix.asClass(GetMovieQueryHandler),
      // QUERY_HANDLERS_SETUP
    ]),
  });

  return container;
}
