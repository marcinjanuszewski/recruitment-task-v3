import { AwilixContainer, Lifetime } from "awilix";
import * as awilix from "awilix";

import { movieRouting } from "../app/features/movie/routing";
// ROUTING_IMPORTS

export async function registerRouting(container: AwilixContainer) {
  container.loadModules(["src/**/*.action.js"], {
    formatName: "camelCase",
    resolverOptions: {
      lifetime: Lifetime.SCOPED,
      register: awilix.asClass,
    },
  });

  container.register({
    movieRouting: awilix.asFunction(movieRouting),
    // ROUTING_SETUP
  });

  return container;
}
