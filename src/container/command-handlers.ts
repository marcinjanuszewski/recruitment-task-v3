import { AwilixContainer } from "awilix";
import * as awilix from "awilix";
import { asArray } from "../shared/awilix-resolvers";

import AddMovieCommandHandler from "../app/features/movie/handlers/add-movie.handler";
// HANDLERS_IMPORTS

export async function registerCommandHandlers(container: AwilixContainer) {
  container.register({
    commandHandlers: asArray<any>([
      awilix.asClass(AddMovieCommandHandler),
      // COMMAND_HANDLERS_SETUP
    ]),
  });

  return container;
}
